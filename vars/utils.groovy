#!/usr/bin/env groovy

def say(String name = 'human') {
    echo "Hello, ${name}."
}
